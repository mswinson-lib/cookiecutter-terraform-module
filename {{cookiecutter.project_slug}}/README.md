# {{cookiecutter.project_name}}

[Terraform Module]({{cookiecutter.terraform_module_home}})} {{cookiecutter.project_summary}}

## Features


## Usage

```HCL
module "{{cookiecutter.project_name}}" {
  source = "{{cookiecutter.terraform_module_home}}"
}
```

## Inputs

| Name | Description | Type | Default | Required


## Outputs

| Name | Description |


