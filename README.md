# terraform-module

    Generates a project for a terraform module


## Requirements

    Install `cookiecutter` , command line: `pip install cookiecutter`


## Usage

    cookiecutter bb:mswinson-lib/cookiecutter-terraform-module.git
    
